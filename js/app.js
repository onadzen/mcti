$(document).foundation();

jQuery(document).ready(function($) {
    $('.panel-grid:has(#row-section)').addClass('grid-container');
    $('.gform_footer .gform_button.button').addClass('normal');
    $('select').selectric();
    $('.news-slider').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left fa-2x"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right fa-2x"></i></button>',
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 640,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    });
});