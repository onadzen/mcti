<?php
// check if the flexible content field has rows of data
if( have_rows('column_sections') ):
     // loop through the rows of data
    while ( have_rows('column_sections') ) : the_row();
        if( get_row_layout() == 'header_section' ): ?>
        	<?php 
				$image = get_sub_field('add_image');
				$txtoverlay = get_sub_field('title_overlay');
			?>
			<div id="frontbanner">
					<div class="text-overlay">
						<?php echo $txtoverlay; ?>
					</div>
				<img src="<?php echo $image; ?>"/> 
			</div>	
		<?php endif; ?>
		
		<?php if( get_row_layout() == 'full_column_section' ): ?>
        	<?php 
				$full_column_editor = get_sub_field('full_column_editor');
			?>
			<div id="full-column" class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="cell">
						<?php echo $full_column_editor; ?>
					</div>
				</div>

				<?php
				// check if the repeater field has rows of data
				if( have_rows('two_column_editor') ):
					echo '<div class="grid-x grid-margin-x">';
					// loop through the rows of data
					while ( have_rows('two_column_editor') ) : the_row();
						echo '<div class="cell medium-6 large-6">';
						// display a sub field value
						the_sub_field('two_column_entry');
						echo '</div>';
					endwhile;
					echo '</div>';
				else :
					// no rows found
				endif; ?>
			</div>	
		<?php endif; ?>

		<?php if( get_row_layout() == 'three_column_section' ): ?>
        	<?php 
				$full_column_editor = get_sub_field('three_column_editor');
			?>
			<div id="three-column-grid" class="grid-container full" style="overflow:hidden;">
				<?php
				// check if the repeater field has rows of data
				if( have_rows('three_column_editor') ): ?>
					<div class="grid-x grid-padding-x">
					<?php // loop through the rows of data
					while ( have_rows('three_column_editor') ) : the_row(); ?>
						<div class="cell medium-4 large-4" style="background: <?php the_sub_field('bg_color'); ?>">
						<?php // display a sub field value
						echo '<h3>'; 
						the_sub_field('column_title');
						echo '</h3>';
						the_sub_field('three_column_entry');
						echo '</div>';
					endwhile;
					echo '</div>';
				else :
					// no rows found
				endif; ?>
			</div>	
		<?php endif; ?>
		
		<?php if( get_row_layout() == 'full_column_section_with_background' ): ?>
		<div id="full-column-background">	
			<div class="grid-container">
				<?php
				// check if the repeater field has rows of data
				if( have_rows('two_column_editor') ):
					echo '<div class="grid-x grid-margin-x">';
					// loop through the rows of data
					while ( have_rows('two_column_editor') ) : the_row();
						echo '<div class="cell medium-6 large-6 right-column-blue">';
						// display a sub field value
						the_sub_field('two_column_entry');
						echo '</div>';
					endwhile;
					echo '</div>';
				else :
					// no rows found
				endif; ?>
			</div>
		</div>		
		<?php endif; ?>	

		<?php if( get_row_layout() == 'news_section' ): 
			$news_background = get_sub_field('news_background'); 
		?>
		<div id="news-section" style="background-color: <?php echo $news_background; ?>">	
			<div class="grid-container">
				<?php 
					//$term = the_sub_field('select_category');
					$args = array(
						'post_type' => 'post',
						//'posts_per_page' => -1,
						//'post__in'  => get_option( 'sticky_posts' ),
						'category_name' => $term->slug,
						'posts_per_page' => -1
					);
					query_posts($args); ?>
					<?php if (have_posts()) : ?>
						<div class="grid-x grid-margin-x text-center">
							<div class="cell">
								<h2><?php the_sub_field('news_section_title'); ?></h2>
							</div>	
						</div>	
						<div class="news-slider">
							<?php while (have_posts()) : the_post(); ?>
							<div class="news-wrapper">
							<div class="media-object stack-for-small stack-for-medium">
								<?php
								// check if the post or page has a Featured Image assigned to it.
								if ( has_post_thumbnail() ) : ?>
								<div class="media-object-section">
									<div class="thumbnail">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">    
											<?php the_post_thumbnail('large', array('class' => 'newsthumb')); ?>
										</a>
									</div>
								</div>
								<?php endif; ?>	
								<div class="media-object-section">
									<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<?php echo "<p>" . excerpt(50) . "</p>"; ?>
									<p><a class="read-on" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">READ ON <i class="fas fa-arrow-right"></i></a></p>
								</div>
							</div>
							</div>
							<?php endwhile; ?>				
						</div>
					<?php endif; 
				wp_reset_query(); ?>
			</div>
		</div>		
		<?php endif; ?>	

    <?php endwhile;
else :
    echo "";
endif; ?>