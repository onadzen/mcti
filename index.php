<?php
/**
 * The template for displaying all posts
 *
 */
get_header(); ?>
	<?php if ( have_posts() ) : ?> 
	<div id="maincontent" class="grid-container">
		<div class="grid-x grid-margin-x">
		<div class="cell">
			<?php echo "<h1>Cleaning Industry News</h1>"; ?>
		</div>
		</div>
		<div class="grid-x grid-padding-x medium-up-2 large-up-2 news-wrapper">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="cell">
				<?php get_template_part( 'template-parts/post/content', get_post_format() ); ?>
			</div>
		<?php endwhile; // End of the loop. ?>		
		</div>
	</div><!-- .maincontent -->
	<?php endif; ?>
<?php get_footer();