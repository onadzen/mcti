<?php
/**
 * The template for displaying the footer
 */
?>
			<?php if ( is_active_sidebar( 'top-footer-widget' ) ) : ?>
			<div class="footerwidget">
				<div class="grid-container">
					<div class="grid-x grid-margin-x">
						<div class="cell">
							<hr/>
							<?php dynamic_sidebar( 'top-footer-widget' ); ?>
						</div>	
					</div>	
				</div>
			</div>
			<?php endif; ?>
			<footer>
				<div class="grid-container">
					<div class="grid-x grid-margin-x text-center">
						<div class="cell">
							<a href="#"><img src="<?php bloginfo( 'template_directory' )?>/images/logo.jpg" alt=""></a>
						</div>
						<div class="cell copyright">
							<p>&copy; <?php bloginfo('name');?>. All rights reserved.</p>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>
</div>
<?php wp_footer(); ?>
<script>try{Typekit.load();}catch(e){}</script>
<script>
</body>
</html>
