<?php
/**
 * The template for displaying all pages
 *
 */
get_header(); ?>
	<?php
	if ( have_posts() ) : 
		while ( have_posts() ) : the_post(); ?>
			<?php
			// check if the post or page has a Featured Image assigned to it.
			if ( has_post_thumbnail() ) : ?>
				<div class="page-banner">
					<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>	
		<div id="maincontent" class="grid-container">
			<div class="grid-x grid-margin-x">

				<div class="cell">
					<?php get_template_part( 'template-parts/page/content', 'page' ); ?>
				</div>
			</div>	
		</div><!-- .maincontent -->
		<?php 
		endwhile; // End of the loop.
	endif; ?>
	<?php get_template_part( 'includes/section' ); ?>
<?php get_footer();
