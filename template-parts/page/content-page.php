<?php
/**
 * Template part for displaying page content in page.php
 */ ?>
		 <div class="page-entry" <?php post_class(); ?> id="post-<?php the_ID(); ?>">
				<?php //the_title( '<h2 class="entry-title">', '</h2>' ); ?>
				<?php the_content(); ?>
		</div>