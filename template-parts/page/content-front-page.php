<?php
/**
 * Displays content for front page
 */

?>
		 <div class="page-entry" <?php post_class(); ?> id="post-<?php the_ID(); ?>">
		 		<?php //the_title( '<h1>', '</h1>' ); ?>
				<?php the_content(); ?>
		</div>
