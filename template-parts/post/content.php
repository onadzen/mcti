<?php
/**
 * Template part for displaying posts
  */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} elseif ( is_home() ) {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		} else {
			echo "";
	} ?>
	<div class="media-object stack-for-small">
  		<div class="media-object-section">
		  	<?php // check if the post has a Post Thumbnail assigned to it.
			if ( has_post_thumbnail() ) :
				echo '<div class="thumbnail">'; ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php the_post_thumbnail('medium'); ?>
					</a>
				<?php echo '</div>';	
			endif; ?>
    	</div>
  		<div class="media-object-section">
		  	<?php
				/* limit excerpt words */
				echo "<p>" .excerpt(40);
			?>
			<a class="read-on" href="<?php the_permalink(); ?>">READ ON <i class="fas fa-angle-right"></i></a> </p>
  		</div>
	</div>
</article><!-- #post-## -->
