<?php
/**
 * Template part for displaying a message that posts cannot be found
 */

?>
<div id="maincontent" class="grid-container">
	<div class="grid-x grid-margin-x align-center">
		<div class="cell auto">
			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Nothing Found' ); ?></h1>
			</header>
			<div class="page-not-found">
				<?php
				if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>
					<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentyseventeen' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>
				<?php else : ?>
					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyseventeen' ); ?></p>
				<?php
					get_search_form();
				endif; ?>
			</div><!-- .page-content -->
		</div>
	</div>
</div>	
