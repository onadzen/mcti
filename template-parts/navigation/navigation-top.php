<?php
/**
 * Displays top navigation
 */

?>

    <div data-sticky-container>
    <nav class="navwrapper">
    <div class="title-bar sticky" data-top-anchor="1" data-options="marginTop:0;" style="width:100%" data-sticky>
        <div class="title-bar-title">
              <?php
                //if ( function_exists( 'the_custom_logo' ) ) :
                //the_custom_logo();
                //endif; 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                  echo '<a href='. get_bloginfo( 'url' ) .'><img src="'. esc_url( $logo[0] ) .'"></a>';
                } else {
                  echo get_bloginfo( 'name' );
                } ?>
        </div>
        <div class="title-bar-left top-menu">
              <?php wp_nav_menu( array(
                'theme_location' => 'main-menu',
                'container' => 'ul',
                'menu_id'        => 'main-menu',
                'menu_class'	 => 'vertical medium-horizontal menu',
                'items_wrap'     => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown" style="width: 100%;">%3$s</ul>',
                //Recommend setting this to false, but if you need a fallback...
                'fallback_cb'    => 'f6_drill_menu_fallback',
                'walker'         => new F6_DRILL_MENU_WALKER()
              ) ); ?>
        </div>
        <div class="title-bar-right mobile-icons">
              <div id="topbar-mobile-search" class="topbar-search">
                <?php get_search_form();  ?>
              </div>
              <span class="mobile-menu">  
                <button class="mobile-icon" type="button" data-open="offCanvas"><i class="fas fa-bars fa-lg"></i></button>
              </span>
              <span class="mobile-search" data-responsive-toggle="topbar-mobile-search" data-hide-for="large">
                <button class="search-icon" type="button" data-toggle><i class="fas fa-search fa-lg"></i></button>
              </span>
        </div> 
      </div>
      </nav> 
      </div>
         