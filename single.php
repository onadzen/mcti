<?php
/**
 * The template for displaying single post
 *
 */
get_header(); ?>
	<?php
	if ( have_posts() ) : 
	while ( have_posts() ) : the_post(); ?>
	<div id="maincontent" class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="cell">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
					if ( is_single() ) {
						the_title( '<h1 class="entry-title">', '</h1>' );
					} elseif ( is_home() ) {
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					} else {
						echo "";
					} ?>
					<p class="date-posted"><?php the_date(); ?></p>
					<?php echo the_content(); ?>
					<p class="backtopage"><a href="javascript:history.go(-1)" title="Back to previous page"><i class="fas fa-angle-left"></i>&nbsp;Back</a></p>
				</article><!-- #post-## -->
			</div>
		</div>	
	</div><!-- .maincontent -->
	<?php 
	endwhile; // End of the loop.
	endif; ?>
<?php get_footer();