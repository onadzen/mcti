<form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="search-box">
        <label class="screen-reader-text" for="s">Search for:</label>
        <input placeholder="Type search, hit enter" value="" name="s" id="s" type="text">
        <input id="searchsubmit" value="Search" type="submit">
    </div>
</form>