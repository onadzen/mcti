<?php
// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'init_theme', 16 );
function init_theme() {
    // launching operation cleanup
    add_action( 'init', 'head_cleanup' );
    //add custom menu
	add_action('init', 'register_custom_menu');
	// remove WP version from RSS
    add_filter( 'the_generator', 'rss_version' );
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action( 'wp_head', 'remove_recent_comments_style', 1 );
    // clean up gallery output in wp
    add_filter( 'gallery_style', 'gallery_style' );
    // enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'scripts_and_styles', 999 );
	add_theme_support( 'post-thumbnails' );	
	//add_post_type_support( 'post_type', 'woosidebars' );
} /* end  */
/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/
function head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );
	// Remove Admin bar
	add_filter('show_admin_bar', 'remove_admin_bar');
} /* end head cleanup */
// Remove Admin bar
function remove_admin_bar()
{
    return false;
}
// register menu locations
function register_custom_menu() {
	register_nav_menu('main-menu', __('Main Menu'));
	register_nav_menu('footer-menu', __('Footer Menu'));
}
// remove WP version from RSS
function rss_version() { return ''; }
// remove WP version from scripts
function remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
// remove injected CSS for recent comments widget
function remove_wp_widget_recent_comments_style() {
   if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
      remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
   }
}

//add logo function 
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 117,
        'width'       => 382,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );

// remove injected CSS from recent comments widget
function remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}
// remove injected CSS from gallery
function gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}
/*********************
SCRIPTS & ENQUEUEING
*********************/
// loading modernizr and jquery, and reply script
function scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

	// register foundation stylesheet
    wp_register_style( 'mainstyle', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all' );
    wp_register_style( 'foundation', get_stylesheet_directory_uri() . '/css/app.css', array(), '', 'all' );
	wp_register_style( 'fontawesome-solid', 'https://use.fontawesome.com/releases/v5.0.13/css/solid.css' , array(), '', 'all' );
	wp_register_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css' , array(), '', 'all' );
    //register custom styles sheets
	wp_register_style( 'selectric-css', get_stylesheet_directory_uri() . '/css/selectric.css', array(), '', 'all' );
	wp_register_style( 'slick-css', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '', 'all' );
	wp_register_style( 'custom-styles', get_stylesheet_directory_uri() . '/css/custom-style.css', array(), '', 'all' );
    

    // comment reply script for threaded comments
    //if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
	//	wp_enqueue_script( 'comment-reply' );
    //}

    //adding scripts file in the footer
    // Drop this in functions.php or your theme
	if( !is_admin()){
		wp_deregister_script('jquery');
		//	wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.1', true );
		wp_register_script( 'jquery', get_template_directory_uri() . '/bower_components/jquery/dist/jquery.js', array(), '', true );
		wp_register_script( 'what-input-js', get_template_directory_uri() . '/bower_components/what-input/dist/what-input.js', array(), '', true );
		wp_register_script( 'foundation-js', get_template_directory_uri() . '/bower_components/foundation-sites/dist/js/foundation.min.js', array(), '', true );
		wp_register_script( 'selectric-js', get_template_directory_uri() . '/js/jquery.selectric.min.js', array(), '', true );
		wp_register_script( 'slick-js', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), '', true );
		wp_register_script( 'app-js', get_template_directory_uri() . '/js/app.js', array(), '', true );
	}
	// enqueue styles
	wp_enqueue_style( 'foundation');
	wp_enqueue_style( 'fontawesome');
	wp_enqueue_style( 'fontawesome-solid');
	wp_enqueue_style( 'selectric-css');
	wp_enqueue_style( 'slick-css');
	wp_enqueue_style( 'mainstyle');
	// enqueue scripts
	wp_enqueue_script('jquery');
	wp_enqueue_script('what-input-js');
	wp_enqueue_script('foundation-js');
	wp_enqueue_script('selectric-js');
	wp_enqueue_script('slick-js');
	wp_enqueue_script('app-js');
  	
    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
  }
}
/************* ACTIVE WIDGETS ********************/
// Widgetizes Areas
function theme_widgets_init() {
	register_sidebar(array(
		'name'=>'Top Menu Widget',
		'id' => 'top-menu-widget',
		'description' => __( 'This is the top menu widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name'=>'Top Footer Widget',
		'id' => 'top-footer-widget',
		'description' => __( 'This is the top footer widget' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));
}
// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action( 'widgets_init', 'theme_widgets_init' );

class F6_DRILL_MENU_WALKER extends Walker_Nav_Menu {
	/*
	 * Add vertical menu class
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat( "\t", $depth );
		$output .= "\n$indent<ul class=\"vertical menu\">\n";
	}
}

function f6_drill_menu_fallback( $args ) {
	/*
	 * Instantiate new Page Walker class instead of applying a filter to the
	 * "wp_page_menu" function in the event there are multiple active menus in theme.
	 */
	$walker_page = new Walker_Page();
	$fallback    = $walker_page->walk( get_pages(), 0 );
	$fallback    = str_replace( "children", "children vertical menu", $fallback );
	echo '<ul class="menu vertical nested">' . $fallback . '</ul>';
}

// remove LINKS menu item
add_action( 'admin_menu', 'my_admin_menu' );

function my_admin_menu() {
     remove_menu_page('link-manager.php');
}

//add data hover to menu items
add_filter( 'nav_menu_link_attributes', 'add_custom_data_atts_to_nav', 10, 4 );
function add_custom_data_atts_to_nav( $atts, $item, $args ) {

$atts['data-hover'] = $item->title;
return $atts;}

//hook the administrative header output
//add_action('admin_head', 'my_custom_logo');
//function my_custom_logo() {
//   echo '
//      <style type="text/css">
//         #header-logo { background-image: url('.get_bloginfo('template_directory').'/images/custom-logo.gif) !important; }
//      </style>
//   ';
//}

// limit words excerpt and content
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
	  array_pop($excerpt);
	  $excerpt = implode(" ",$excerpt).'';
	} else {
	  $excerpt = implode(" ",$excerpt);
	}	
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
function content($limit) {
	$content = explode(' ', get_the_content(), $limit);
	if (count($content)>=$limit) {
	  array_pop($content);
	  $content = implode(" ",$content).'...';
	} else {
	  $content = implode(" ",$content);
	}	
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content); 
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}

//create a hidden selector on Appearance tab using to remove Gravity form labels 
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*function to add async and defer attributes*/
function defer_js_async($tag){

## 1: list of scripts to defer.
$scripts_to_defer = array();
## 2: list of scripts to async.
$scripts_to_async = array();
 
#defer scripts
foreach($scripts_to_defer as $defer_script){
	if(true == strpos($tag, $defer_script ) )
	return str_replace( ' src', ' defer="defer" src', $tag );	
}
#async scripts
foreach($scripts_to_async as $async_script){
	if(true == strpos($tag, $async_script ) )
	return str_replace( ' src', ' async="async" src', $tag );	
}
return $tag;
}
add_filter( 'script_loader_tag', 'defer_js_async', 10 );